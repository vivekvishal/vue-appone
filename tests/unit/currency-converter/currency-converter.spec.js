import { shallowMount, mount } from '@vue/test-utils';
import currencyConverter from '@/components/currency-converter/currency-converter.vue';
import * as httpUtils from '../../../src/utilities/http.js';

describe('currency-converter', () => {
  let wrapper, httpGetStub;

  beforeEach(() => {
    httpGetStub = jest.spyOn(httpUtils, 'httpGet');
    httpGetStub.mockResolvedValue({"rates":{"INR":20},"base":"EUR","date":"2019-09-06"});
  });

  afterEach(() => {
    httpGetStub.mockRestore();
  });

  describe('upon initialization', () => {

    beforeEach(() => {
      wrapper = shallowMount(currencyConverter);
    });

    it('wrapper should exist', () => {
      expect(wrapper).not.toBeUndefined();
      expect(wrapper).toBeDefined();
    });

    it('should call the api service once  to fetch deafult currency rates ', ()=> {
      expect(httpUtils.httpGet).toHaveBeenCalledTimes(1);
    })

  });

  describe('when getting a currency value with base and target', () => {

    beforeEach(() => {
      wrapper = shallowMount(currencyConverter);
    });

    it('should set the default base currency as EUR', () => {
      expect(wrapper.vm.baseCurrency).toEqual('EUR');
    })

    it(' target currency value should be defined', () => {
      expect(wrapper.vm.targetCurrency).toBeDefined();
    });

    it('target currency value should be defined and defaulted as GBP', () => {
      expect(wrapper.vm.targetCurrency).toEqual('GBP');
    });

    it('should return conversion value when called the conversion method', async () => {
      wrapper.vm.baseCurrency = 'EUR';
      wrapper.vm.targetCurrency = 'INR'
      httpGetStub.mockResolvedValue({"rates":{"INR":20},"base":"EUR","date":"2019-09-06"});
      await wrapper.vm.getConversionRateByCurrency();
      expect(wrapper.vm.conversionValue).toEqual(20);
    });

    it('should reset the conversion value when called the method', ()=> {
      wrapper.vm.resetConversionValue();
      expect(wrapper.vm.conversionValue).toEqual(0);
    })


  });

});
