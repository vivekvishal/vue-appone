import { shallowMount } from '@vue/test-utils';
import MainApp from '@/components/main.vue';

describe('MainApp.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message';
    const wrapper = shallowMount(MainApp, {
      propsData: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
