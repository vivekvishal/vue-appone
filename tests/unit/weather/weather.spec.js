import { shallowMount, mount } from '@vue/test-utils';
import Weather from '@/components/Weather/Weather.vue';
import * as httpUtils from '../../../src/utilities/http.js';

describe('weather app test suite', () => {
  let wrapper, httpGetStub;

  beforeEach(() => {
    httpGetStub = jest.spyOn(httpUtils, 'httpGet');
    httpGetStub.mockResolvedValue({name:'Amsterdam', weather:[{icon:'test'}], main:{humidity:'30'}, wind:{speed:'30'}});
  });

  afterEach(() => {
    httpGetStub.mockRestore();
  });

  describe('upon initialization', () => {

    beforeEach(async () => {
      wrapper = shallowMount(Weather);
    });

    it('wrapper should exist', () => {
      expect(wrapper).not.toBeUndefined();
      expect(wrapper).toBeDefined();
    })

    it('should call the api service once  to fetch weather details ', ()=> {
      expect(httpUtils.httpGet).toHaveBeenCalledTimes(1);
    })

    it('should show the city name as location on the screen', ()=> {
      expect(wrapper.vm.location).toEqual('Amsterdam');
    });
    it('should show the correct humidity numbers', ()=> {
      expect(wrapper.vm.humidity).toEqual('30');
    });
    it('should show the correct wind speed on the screen', ()=> {
      expect(wrapper.vm.wind).toEqual('30');
    });

  });

});