import { shallowMount } from '@vue/test-utils';
import counter from '@/components/counter/counter.vue';

describe('counter.vue', () => {
let wrapper;
let msg = "testing message"; 
  beforeEach(()=> {
    wrapper = shallowMount(counter, {
      propsData: { msg },
    });
  });

  it('should have default counter value as zero', ()=> {
    expect(wrapper.vm.count).toEqual(0);
  })

  it('should increment the counter when called method - increment ', ()=> {
    wrapper.vm.increment();
    expect(wrapper.vm.count).toEqual(1);
  })

  it('should render the props message when increment', () => {
    wrapper.vm.increment();
    expect(wrapper.vm.message).toEqual(wrapper.vm.msg);
    expect(wrapper.vm.message).toEqual('testing message');
    expect(wrapper.text()).toContain(msg);
  });
  
  it('should reset the counter', () => {
    wrapper.vm.reset();
    expect(wrapper.vm.count).toEqual(0);
  });

  afterEach(()=> {
    wrapper.destroy();
  });

 

  

});
