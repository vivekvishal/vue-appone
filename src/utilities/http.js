const httpGet = async (url, headers) => {
  const requestOptions = {
    method: 'GET',
  };
  try {
    const response = await fetch(`${url}`, requestOptions);
    return handleResponse(response.json());
  } catch (error) {
    console.log("there was some error " + error)
  }

};

const handleResponse = (responsePromise) => {
  try {
    return responsePromise.then(data => data);
  } catch (error) {
    console.log(error);
  }
}

export { httpGet };
